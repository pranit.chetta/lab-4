#include <stdio.h>
#include <stdbool.h>
#include <omp.h>
#include <time.h>

#define MAX 7

int intArray[MAX] = {5,8,4,15,6,12,14};

void printline(int count) {
   int i;
	
   for(i = 0;i < count-1;i++) {
   }
	
}

void display() {
   int i;
   printf("[");
	
   for(i = 0;i < MAX;i++) {
      printf("%d ",intArray[i]);
   }
	
   printf("]\n");
}

void swap(int n1, int n2) {
   int temp = intArray[num1];
   intArray[n1] = intArray[n2];
   intArray[n2] = temp;
}

int partition(int left, int right, int pivot) {
   int leftPointer = left -1;
   int rightPointer = right;

   while(true) {
        #pragma omp parallel sections num_threads(3)
        {
            #pragma omp section{
                while(intArray[++leftPointer] < pivot) {
         
                }
            }

            #pragma omp section{
                while(rightPointer > 0 && intArray[--rightPointer] > pivot) {
         
                }
            }

            #pragma omp section{
                 if(leftPointer >= rightPointer) {
                    break;
                } else {
                    printf(" item :%d,%d\n", intArray[leftPointer],intArray[rightPointer]);
                    swap(leftPointer,rightPointer);
                }
            }
        }
   }
	
   printf(" pivot :%d,%d\n", intArray[leftPointer],intArray[right]);
   swap(leftPointer,right);
   printf("Updated Array: "); 
   display();
   return leftPointer;
}

void quickSort(int left, int right) {
   if(right-left <= 0) {
      return;   
   } else {
      int pivot = intArray[right];
      int partitionPoint = partition(left, right, pivot);
      quickSort(left,partitionPoint-1);
      quickSort(partitionPoint+1,right);
   }        
}

int main() {
   printf("Input Array: ");
   display();
   printline(50);
   quickSort(0,MAX-1);
   printf("Output Array: ");
   display();
   printline(50);
}